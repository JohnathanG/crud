<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pessoas extends CI_Controller {

    public function __construct() {

            parent::__construct();
            $this->load->model("pessoas_model"); 
    }

    public function create() {
    	$datatitulo['title'] = 'Cadastrar Pessoas';
        
        $dados['link']       = 'home';
        $dados['objeto']     = new Pessoas_model();

        $this->pessoas_model->validar();

        if ($this->form_validation->run() === FALSE) {
	        $this->load->view('templates/header', $datatitulo);
	        $this->load->view('pages/home', $dados);
	        $this->load->view('templates/mensagem');
	        $this->load->view('templates/footer');
        } else {
            $this->pessoas_model->set();

            $this->session->set_flashdata('msg', array('tipo'=>'success','mensagem'=>'Salvo'));
            redirect( base_url('home'));
        }
    }

    public function getByCPF($cpf = null) {
    	$data['title']              = 'Consultar Pessoas';
        $data['link']               = 'consultar';

        $dados['dados']    			= $this->pessoas_model->getByCPF($this->input->post('cpf'));

        $this->pessoas_model->validarCPF();

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('templates/header', $data);
            $this->load->view('pages/consultar', $dados);
            $this->load->view('templates/mensagem');
            $this->load->view('templates/footer');
        } else {

        	if(count($dados['dados']) >= 1){

        		$this->session->set_flashdata('msg', array('tipo'=>'success','mensagem'=>'Cadastro com este CPF encontrado!'));
        		redirect( base_url('home/edit/'.$dados['dados'][0]->id));
        	}else{
        		$this->session->set_flashdata('msg', array('tipo'=>'error','mensagem'=>'Cadastro com este CPF não foi encontrado!'));
        		redirect( base_url('consultar'));
        	}

        }
    }

    public function edit($id = 0){
        
        $data['title']                  = 'Editar Pessoa';
        $data['link']                   = 'home/edit/'.$id;
        $dados['objeto']                = $this->pessoas_model->get($id, '*, if(nascimento = "0000-00-00", "", DATE_FORMAT(nascimento, "%d/%m/%Y")) AS nascimento')[0];
        
        $this->pessoas_model->validar();
        
        if($this->form_validation->run() === FALSE) {

            $this->load->view('templates/header', $data);
            $this->load->view('pages/home', $dados);
            $this->load->view('templates/footer');

        }else{

        	switch ($_POST["msg"]) {
        	    case 'Salvar':
        	        $this->pessoas_model->set($id); 

        	        $this->session->set_flashdata('msg', array('tipo'=>'success','mensagem'=>'Salvo'));
        	        redirect( base_url('home'));

        	        break;

        	    case 'Apagar':
					//
        	    	echo "<script>
							var conf = confirm('Tem certeza que deseja apagar esta Pessoa?');
							if(conf){
								window.location.href='".base_url()."home/delete/".$id."';
							}else{
								window.location.href='".base_url()."home/deleteCancelado/".$id."';
							}
						</script>";
        	        break;

        	    default:
        	        //no action sent
        	}

        }
    }

    public function deleteCancelado($id = 0){
    	$this->session->set_flashdata('msg', array('tipo'=>'error','mensagem'=>'Cancelado!'));
        		redirect( base_url('home/edit/'.$id));
    }

    public function delete($id = 0){
    	$this->pessoas_model->delete($id);

    	$this->session->set_flashdata('msg', array('tipo'=>'success','mensagem'=>'Cadastro eliminado com sucesso!'));
        		redirect( base_url('home'));
    }

}