    <footer class="footer">
        <div class="col-md-12 text-center">
          <div class="pt-1">
            <p>
              Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos os direitos reservados
            </p>
          </div>
        </div>
      </div>
    </footer>

    <!--   Core JS Files   -->
    <script src="<?= base_url() ?>assets/js/core/jquery.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/js/core/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/js/jquery.maskedinput.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/js/principal.js" type="text/javascript"></script>
    <script src="<?= base_url("assets/js/alertify.js") ?>"></script>

    <script type="text/javascript">
        var mensagem = <?= json_encode($this->session->flashdata('msg')); ?>;
        if(mensagem !== null){
            switch(mensagem['tipo']){
                case ("success"):
                    alertify.success(mensagem['mensagem']);
                    break;
                case ("error"):
                    alertify.error(mensagem['mensagem']);
                    break;
            }
        }
    </script>
  </body>
</html>
