<!DOCTYPE html>
<html lang="pt-br">
  <head>
    
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="<?= base_url() ?>assets/img/logo.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">


    <title><?= $title ?></title>
    <!--Bootstrap core CSS-->
    <link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/principal.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/alertify.css">

  </head>
  <body>

    <header>
      <div class="container fixed-top p-0">
        <nav class="navbar navbar-expand-md navbar-light bg-light pt-2 pb-0">
          <a class="navbar-brand" href="<?= base_url() ?>">
            <img src="<?= base_url('assets/img/logo.png') ?>" height="50" alt="">
          </a>
          Nome da Empresa
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item active">
                <a class="nav-link" href="<?= base_url() ?>">Cadastrar <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?= base_url('consultar') ?>">Consultar</a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </header>
    <div id="corpo" class="container py-4">