
<div id="mensagem" class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4><?= $title; ?></h4>
                </div>
                <div class="panel-body pt-4">
                    <?php if (validation_errors()): ?>
                      <div class="alert alert-warning alert-dismissible" role="alert">
                           <?php echo validation_errors(); ?>
                     </div>
                    <?php endif; ?>
                    <?php echo form_open($link, array('id'=>'cadastro')); ?>

                    <div class="tab-content">
                        <div id="pessoal" class="">

                            <input type="hidden" value="<?= $objeto->id ?>" name="id" id="id">
                                
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="nome" class="form-label">Nome</label>
                                    <input class="form-control" type="text" value="<?= set_value('nome', $objeto->nome) ?>" name="nome" id="nome" placeholder="Nome completo" >
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-2">
                                    <label for="sexo" class="form-label">Sexo</label>
                                    <select class="form-control selectpicker" id="sexo" name="sexo" title="Escolha uma opção abaixo">
                                        <option></option>
                                        <option value="Masculino" <?= set_select('sexo', 'Masculino', ($objeto->sexo == 'Masculino')) ?>>Masculino</option>
                                        <option value="Feminino" <?= set_select('sexo', 'Feminino', ($objeto->sexo == 'Feminino')) ?>>Feminino</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="nascimento" class="form-label">Nascimento</label>
                                    <input class="form-control" type="text" size="10" maxlength="10" value="<?= set_value('nascimento', $objeto->nascimento) ?>" name="nascimento" id="nascimento" placeholder="Ex.: 01/01/1970">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="cpf" class="form-label">CPF</label>
                                    <input class="form-control" type="text" maxlength="14" value="<? 
                                        if(empty($cpf)){
                                            echo $objeto->cpf;
                                        }else{
                                            echo $cpf;
                                        } 
                                    ?>" name="cpf" id="cpf" <? if($objeto->id > 0 && 
                                        str_replace(" ", "", str_replace("-", "", str_replace(".", "", 
                                        $objeto->cpf))) != "") echo('readonly="true"'); ?> placeholder="Somente números">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="identidadeNumero" class="form-label">RG</label>
                                    <input class="form-control" type="text" value="<?= set_value('identidadeNumero', $objeto->identidadeNumero) ?>" name="identidadeNumero" id="identidadeNumero">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="identidadeOrgaoExpeditor" class="form-label">Orgão Exp.</label>
                                    <input class="form-control" type="text" value="<?= set_value('identidadeOrgaoExpeditor', $objeto->identidadeOrgaoExpeditor) ?>" name="identidadeOrgaoExpeditor" id="identidadeOrgaoExpeditor">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-2">
                                    <label for="enderecoEstado" class="form-label">UF Exp.</label>
                                    <select class="form-control" id="identidadeEstadoExpeditor" name="identidadeEstadoExpeditor">
                                        <option></option>
                                        <option value="AC" <?= set_select('identidadeEstadoExpeditor', 'AC', ($objeto->identidadeEstadoExpeditor == 'AC')) ?>>Acre</option>
                                        <option value="AL" <?= set_select('identidadeEstadoExpeditor', 'AL', ($objeto->identidadeEstadoExpeditor == 'AL')) ?>>Alagoas</option>
                                        <option value="AP" <?= set_select('identidadeEstadoExpeditor', 'AP', ($objeto->identidadeEstadoExpeditor == 'AP')) ?>>Amapá</option>
                                        <option value="AM" <?= set_select('identidadeEstadoExpeditor', 'AM', ($objeto->identidadeEstadoExpeditor == 'AM')) ?>>Amazonas</option>
                                        <option value="BA" <?= set_select('identidadeEstadoExpeditor', 'BA', ($objeto->identidadeEstadoExpeditor == 'BA')) ?>>Bahia</option>
                                        <option value="CE" <?= set_select('identidadeEstadoExpeditor', 'CE', ($objeto->identidadeEstadoExpeditor == 'CE')) ?>>Ceará</option>
                                        <option value="DF" <?= set_select('identidadeEstadoExpeditor', 'DF', ($objeto->identidadeEstadoExpeditor == 'DF')) ?>>Distrito Federal</option>
                                        <option value="ES" <?= set_select('identidadeEstadoExpeditor', 'ES', ($objeto->identidadeEstadoExpeditor == 'ES')) ?>>Espírito Santo</option>
                                        <option value="GO" <?= set_select('identidadeEstadoExpeditor', 'GO', ($objeto->identidadeEstadoExpeditor == 'GO')) ?>>Goiás</option>
                                        <option value="MA" <?= set_select('identidadeEstadoExpeditor', 'MA', ($objeto->identidadeEstadoExpeditor == 'MA')) ?>>Maranhão</option>
                                        <option value="MT" <?= set_select('identidadeEstadoExpeditor', 'MT', ($objeto->identidadeEstadoExpeditor == 'MT')) ?>>Mato Grosso</option>
                                        <option value="MS" <?= set_select('identidadeEstadoExpeditor', 'MS', ($objeto->identidadeEstadoExpeditor == 'MS')) ?>>Mato Grosso do Sul</option>
                                        <option value="MG" <?= set_select('identidadeEstadoExpeditor', 'MG', ($objeto->identidadeEstadoExpeditor == 'MG')) ?>>Minas Gerais</option>
                                        <option value="PA" <?= set_select('identidadeEstadoExpeditor', 'PA', ($objeto->identidadeEstadoExpeditor == 'PA')) ?>>Pará</option>
                                        <option value="PB" <?= set_select('identidadeEstadoExpeditor', 'PB', ($objeto->identidadeEstadoExpeditor == 'PB')) ?>>Paraíba</option>
                                        <option value="PR" <?= set_select('identidadeEstadoExpeditor', 'PR', ($objeto->identidadeEstadoExpeditor == 'PR')) ?>>Paraná</option>
                                        <option value="PE" <?= set_select('identidadeEstadoExpeditor', 'PE', ($objeto->identidadeEstadoExpeditor == 'PE')) ?>>Pernambuco</option>
                                        <option value="PI" <?= set_select('identidadeEstadoExpeditor', 'PI', ($objeto->identidadeEstadoExpeditor == 'PI')) ?>>Piauí</option>
                                        <option value="RJ" <?= set_select('identidadeEstadoExpeditor', 'RJ', ($objeto->identidadeEstadoExpeditor == 'RJ')) ?>>Rio de Janeiro</option>
                                        <option value="RN" <?= set_select('identidadeEstadoExpeditor', 'RN', ($objeto->identidadeEstadoExpeditor == 'RN')) ?>>Rio Grande do Norte</option>
                                        <option value="RS" <?= set_select('identidadeEstadoExpeditor', 'RS', ($objeto->identidadeEstadoExpeditor == 'RS')) ?>>Rio Grande do Sul</option>
                                        <option value="RO" <?= set_select('identidadeEstadoExpeditor', 'RO', ($objeto->identidadeEstadoExpeditor == 'RO')) ?>>Rondônia</option>
                                        <option value="RR" <?= set_select('identidadeEstadoExpeditor', 'RR', ($objeto->identidadeEstadoExpeditor == 'RR')) ?>>Roraima</option>
                                        <option value="SC" <?= set_select('identidadeEstadoExpeditor', 'SC', ($objeto->identidadeEstadoExpeditor == 'SC')) ?>>Santa Catarina</option>
                                        <option value="SP" <?= set_select('identidadeEstadoExpeditor', 'SP', ($objeto->identidadeEstadoExpeditor == 'SP')) ?>>São Paulo</option>
                                        <option value="SE" <?= set_select('identidadeEstadoExpeditor', 'SE', ($objeto->identidadeEstadoExpeditor == 'SE')) ?>>Sergipe</option>
                                        <option value="TO" <?= set_select('identidadeEstadoExpeditor', 'TO', ($objeto->identidadeEstadoExpeditor == 'TO')) ?>>Tocantins</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="estadoCivil" class="form-label">Estado Civil</label>
                                    <select class="form-control selectpicker" id="estadoCivil" name="estadoCivil" title="Escolha uma opção abaixo">
                                        <option></option>
                                        <option value="solteiro" <?= set_select('estadoCivil', 'solteiro', ($objeto->estadoCivil == 'solteiro')) ?>>Solteiro(a)</option>
                                        <option value="casado" <?= set_select('estadoCivil', 'casado', ($objeto->estadoCivil == 'casado')) ?>>Casado(a)</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="telefone" class="form-label">Telefone</label>
                                    <input class="form-control" type="text" value="<?= set_value('telefone', $objeto->telefone) ?>" name="telefone" id="telefone">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="celular" class="form-label">Celular</label>
                                    <div class="input-group">
                                        <input class="form-control" type="text" value="<?= set_value('celular', $objeto->celular) ?>" name="celular" id="celular">
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="email_pessoal" class="form-label">Email Pessoal</label>
                                    <div class="input-group">
                                        <input class="form-control" type="text" value="<?= set_value('email_pessoal', $objeto->email_pessoal) ?>" name="email_pessoal" id="email_pessoal" placeholder="Seu email pessoal" >
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="row">
                                
                            </div>
                            <div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="enderecoCep" class="form-label">CEP</label>
                                        <input class="form-control" type="text" max="8" value="<?= set_value('enderecoCep', $objeto->enderecoCep) ?>" name="enderecoCep" id="enderecoCep">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="enderecoRua" class="form-label">Rua</label>
                                        <input class="form-control" type="text" value="<?= set_value('enderecoRua', $objeto->enderecoRua) ?>" name="enderecoRua" id="enderecoRua">
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="enderecoNumero" class="form-label">Número</label>
                                        <input class="form-control" type="text" value="<?= set_value('enderecoNumero', $objeto->enderecoNumero) ?>" name="enderecoNumero" id="enderecoNumero">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="enderecoComplemento" class="form-label">Complemento</label>
                                        <input class="form-control" type="text" value="<?= set_value('enderecoComplemento', $objeto->enderecoComplemento) ?>" name="enderecoComplemento" id="enderecoComplemento">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="enderecoBairro" class="form-label">Bairro</label>
                                        <input class="form-control" type="text" value="<?= set_value('enderecoBairro', $objeto->enderecoBairro) ?>" name="enderecoBairro" id="enderecoBairro">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="enderecoCidade" class="form-label">Cidade</label>
                                        <input class="form-control" type="text" value="<?= set_value('enderecoCidade', $objeto->enderecoCidade) ?>" name="enderecoCidade" id="enderecoCidade">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="enderecoEstado" class="form-label">Estado</label>
                                        <select class="form-control" id="enderecoEstado" name="enderecoEstado">
                                            <option></option>
                                            <option value="AC" <?= set_select('enderecoEstado', 'AC', ($objeto->enderecoEstado == 'AC')) ?>>Acre</option>
                                            <option value="AL" <?= set_select('enderecoEstado', 'AL', ($objeto->enderecoEstado == 'AL')) ?>>Alagoas</option>
                                            <option value="AP" <?= set_select('enderecoEstado', 'AP', ($objeto->enderecoEstado == 'AP')) ?>>Amapá</option>
                                            <option value="AM" <?= set_select('enderecoEstado', 'AM', ($objeto->enderecoEstado == 'AM')) ?>>Amazonas</option>
                                            <option value="BA" <?= set_select('enderecoEstado', 'BA', ($objeto->enderecoEstado == 'BA')) ?>>Bahia</option>
                                            <option value="CE" <?= set_select('enderecoEstado', 'CE', ($objeto->enderecoEstado == 'CE')) ?>>Ceará</option>
                                            <option value="DF" <?= set_select('enderecoEstado', 'DF', ($objeto->enderecoEstado == 'DF')) ?>>Distrito Federal</option>
                                            <option value="ES" <?= set_select('enderecoEstado', 'ES', ($objeto->enderecoEstado == 'ES')) ?>>Espírito Santo</option>
                                            <option value="GO" <?= set_select('enderecoEstado', 'GO', ($objeto->enderecoEstado == 'GO')) ?>>Goiás</option>
                                            <option value="MA" <?= set_select('enderecoEstado', 'MA', ($objeto->enderecoEstado == 'MA')) ?>>Maranhão</option>
                                            <option value="MT" <?= set_select('enderecoEstado', 'MT', ($objeto->enderecoEstado == 'MT')) ?>>Mato Grosso</option>
                                            <option value="MS" <?= set_select('enderecoEstado', 'MS', ($objeto->enderecoEstado == 'MS')) ?>>Mato Grosso do Sul</option>
                                            <option value="MG" <?= set_select('enderecoEstado', 'MG', ($objeto->enderecoEstado == 'MG')) ?>>Minas Gerais</option>
                                            <option value="PA" <?= set_select('enderecoEstado', 'PA', ($objeto->enderecoEstado == 'PA')) ?>>Pará</option>
                                            <option value="PB" <?= set_select('enderecoEstado', 'PB', ($objeto->enderecoEstado == 'PB')) ?>>Paraíba</option>
                                            <option value="PR" <?= set_select('enderecoEstado', 'PR', ($objeto->enderecoEstado == 'PR')) ?>>Paraná</option>
                                            <option value="PE" <?= set_select('enderecoEstado', 'PE', ($objeto->enderecoEstado == 'PE')) ?>>Pernambuco</option>
                                            <option value="PI" <?= set_select('enderecoEstado', 'PI', ($objeto->enderecoEstado == 'PI')) ?>>Piauí</option>
                                            <option value="RJ" <?= set_select('enderecoEstado', 'RJ', ($objeto->enderecoEstado == 'RJ')) ?>>Rio de Janeiro</option>
                                            <option value="RN" <?= set_select('enderecoEstado', 'RN', ($objeto->enderecoEstado == 'RN')) ?>>Rio Grande do Norte</option>
                                            <option value="RS" <?= set_select('enderecoEstado', 'RS', ($objeto->enderecoEstado == 'RS')) ?>>Rio Grande do Sul</option>
                                            <option value="RO" <?= set_select('enderecoEstado', 'RO', ($objeto->enderecoEstado == 'RO')) ?>>Rondônia</option>
                                            <option value="RR" <?= set_select('enderecoEstado', 'RR', ($objeto->enderecoEstado == 'RR')) ?>>Roraima</option>
                                            <option value="SC" <?= set_select('enderecoEstado', 'SC', ($objeto->enderecoEstado == 'SC')) ?>>Santa Catarina</option>
                                            <option value="SP" <?= set_select('enderecoEstado', 'SP', ($objeto->enderecoEstado == 'SP')) ?>>São Paulo</option>
                                            <option value="SE" <?= set_select('enderecoEstado', 'SE', ($objeto->enderecoEstado == 'SE')) ?>>Sergipe</option>
                                            <option value="TO" <?= set_select('enderecoEstado', 'TO', ($objeto->enderecoEstado == 'TO')) ?>>Tocantins</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
                <div class="panel-footer clearfix">
                    <div class="btn-group float-right">
                        <input class="btn btn-md btn-success" type="submit" tipoSubmit="salvar" name="msg" value="Salvar">
                    </div>
                    <?
                        if($objeto->id > 0){

                            echo '<div class="btn-group float-right mr-1">
                                <input class="btn btn-md btn-danger" type="submit" tipoSubmit="apagar" name="msg" value="Apagar">
                            </div>';
                        }
                    ?>
                </div>            
                <?php  echo form_close(); ?>
            </div>
        </div>   
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">AVISO</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modalConteudo">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>