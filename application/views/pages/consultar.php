<script type="text/javascript">
    $(document).ready(function(){
        $("#cpf").focus();
    });
</script>
<div class="container">
   <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title pull-left" style="padding-top: 7.5px;">
                        <?= $title; ?>
                    </h4>
                </div>
                <div class="panel-body pt-4">
                    <?php if (validation_errors()): ?>
                      <div class="alert alert-warning alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <?php echo validation_errors(); ?>
                     </div>
                    <?php endif; ?>
                    <?php echo form_open_multipart($link); ?>
                        <div class="row">
                            <div class="form-group col-md-3">
                                <label for="cpf" class="form-label">CPF</label>
                                <input class="form-control" type="text" maxlength="14" value="" name="cpf" id="cpf" placeholder="Somente números">
                            </div>
                            <div class="col-md-3 form-group">
                                <label for="" class="form-label">Ação </label>
                                <input class="form-control btn btn-success" type="submit" name="submit" value="Consultar" />
                            </div>
                        </div>
                    <?php  echo form_close(); ?>
                </div>
            </div>
        </div>   
    </div>
</div>