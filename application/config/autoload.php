<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array();
$autoload['libraries'] = array('session','database','pagination','form_validation');
$autoload['drivers'] = array();
$autoload['helper'] = array('security','text','url','url_helper','date','form','form_helper');
$autoload['config'] = array();
$autoload['language'] = array();
$autoload['model'] = array();