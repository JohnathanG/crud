<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'pessoas/create';
$route['home'] = 'pessoas/create';
$route['home/edit/(:num)'] = 'pessoas/edit/$1';
$route['home/delete/(:num)'] = 'pessoas/delete/$1';
$route['home/deleteCancelado/(:num)'] = 'pessoas/deleteCancelado/$1';
$route['consultar'] = 'pessoas/getByCPF';

$route['404_override'] = '';

