<?php

class Pessoas_model extends CI_Model{

	public $id = 0;
    public $nome;
    public $sexo;
    public $nascimento;
    public $identidadeNumero;
    public $identidadeOrgaoExpeditor;
    public $identidadeEstadoExpeditor;
    public $cpf;
    public $enderecoRua;
    public $enderecoNumero;
    public $enderecoComplemento;
    public $enderecoBairro;
    public $enderecoCidade;
    public $enderecoEstado;
    public $enderecoCep;
    public $email_pessoal;
    public $telefone;
    public $estadoCivil;
    public $celular;

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function getByCPF($cpf = null)
    {
        $query                   = $this->db->where("p.cpf", $cpf);
        $query                   = $this->db->get("pessoas as p");
        $resultado               = $query->result("Pessoas_model");

        return $resultado;
    }

    public function get($id = null)
    {
        $query                   = $this->db->where("p.id", $id);
        $query                   = $this->db->get("pessoas as p");
        $resultado               = $query->result("Pessoas_model");

        return $resultado;
    }

    public function delete($id = null)
    {
        $this->id                           = $id;

        $this->db->where('id', $this->id);
        $this->db->delete("pessoas", $this);
    }

    public function set($id = 0)
    {   

        $this->id                           = $id;
        $this->nome                         = $this->input->post('nome');
        $this->sexo                         = $this->input->post('sexo');
        if($this->input->post('nascimento') != ""){
            $this->nascimento               = DateTime::createFromFormat('d/m/Y', $this->input->post('nascimento'))->format('Y-m-d');
        } else {
            $this->nascimento               = "0000-00-00";
        }
        $this->identidadeNumero             = $this->input->post('identidadeNumero');
        $this->identidadeOrgaoExpeditor     = $this->input->post('identidadeOrgaoExpeditor');
        $this->identidadeEstadoExpeditor    = $this->input->post('identidadeEstadoExpeditor');
        $this->cpf                          = $this->input->post('cpf');
        $this->enderecoRua                  = $this->input->post('enderecoRua');
        $this->enderecoNumero               = $this->input->post('enderecoNumero');
        $this->enderecoComplemento          = $this->input->post('enderecoComplemento');
        $this->enderecoBairro               = $this->input->post('enderecoBairro');
        $this->enderecoCidade               = $this->input->post('enderecoCidade');
        $this->enderecoEstado               = $this->input->post('enderecoEstado');
        $this->enderecoCep                  = $this->input->post('enderecoCep');
        $this->email_pessoal                = $this->input->post('email_pessoal');
        $this->telefone                     = $this->input->post('telefone');
        $this->celular                      = $this->input->post('celular');
        $this->estadoCivil                  = $this->input->post('estadoCivil');

        if ($this->id !== 0) {
            unset($this->criado);
            $this->db->where('id', $this->id);
            $this->db->update("pessoas", $this);
        } else {
            $this->db->insert("pessoas", $this);
        }
    }

    public function validar(){
        if($this->input->post('id') == 0) {
           $is_unique_identidadeNumero  =  '|is_unique[pessoas.identidadeNumero.id.'.$this->input->post('id').']';
           $is_unique_cpf               =  '|is_unique[pessoas.cpf.id.'.$this->input->post('id').']';
           $is_unique_email             =  '|is_unique[pessoas.email_pessoal.id.'.$this->input->post('id').']';
        } else {
           $is_unique_identidadeNumero  = "";
           $is_unique_cpf               = "";
           $is_unique_email             = "";
        }
        
        $this->form_validation->set_rules('nome',            'Nome',             'required|min_length[8]|max_length[255]|trim|xss_clean');
        $this->form_validation->set_rules('sexo',            'Sexo',             'required|trim|xss_clean');
        $this->form_validation->set_rules('nascimento',      'Nascimento',       'required|trim|xss_clean');
        $this->form_validation->set_rules('identidadeNumero','Número do RG',     'required|max_length[40]|trim|xss_clean'.$is_unique_identidadeNumero);
        $this->form_validation->set_rules('identidadeOrgaoExpeditor','Órgão Expeditor do RG', 'required|max_length[40]|trim|xss_clean');
        $this->form_validation->set_rules('identidadeEstadoExpeditor','Estado Expeditor do RG', 'required|max_length[40]|trim|xss_clean');
        $this->form_validation->set_rules('cpf',             'CPF',              'required|max_length[14]|trim|xss_clean'.$is_unique_cpf);
        $this->form_validation->set_rules('enderecoRua',     'Rua',              'required|min_length[5]|max_length[255]|trim|xss_clean');
        $this->form_validation->set_rules('enderecoNumero',  'Número',           'max_length[10]|trim|xss_clean');
        $this->form_validation->set_rules('enderecoComplemento','Complemento',   'max_length[255]|trim|xss_clean');
        $this->form_validation->set_rules('enderecoBairro',  'Bairro',           'required|max_length[255]|trim|xss_clean');
        $this->form_validation->set_rules('enderecoCidade',  'Cidade',           'required|max_length[255]|trim|xss_clean');
        $this->form_validation->set_rules('enderecoEstado',  'Estado',           'required|max_length[255]|trim|xss_clean');
        $this->form_validation->set_rules('enderecoCep',     'CEP',              'required|max_length[255]|trim|xss_clean');
        $this->form_validation->set_rules('telefone',        'Telefone',         'min_length[8]|max_length[255]|trim|xss_clean');
        $this->form_validation->set_rules('celular',         'Celular',          'min_length[8]|max_length[255]|trim|xss_clean');
        
        $this->form_validation->set_rules('email_pessoal',   'E-mail Pessoal',   'valid_emails'.$is_unique_email);
    }

    public function validarCPF(){
        $this->form_validation->set_rules('cpf',                'CPF',                    'required|max_length[14]|valid_cpf|trim|xss_clean');
    }
}