$(document).ready(function(){

    $("#cpf").mask("999.999.999-99");
    $("#nascimento").mask("99/99/9999");
    $("#telefone").mask("(99)9999-9999");
    $("#celular").mask("(99)99999-9999");
    $("#enderecoCep").mask("99999-999");

    //Quando o campo cep perde o foco.
    $("#enderecoCep").blur(function() {

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $("#enderecoRua").val("...");
                $("#enderecoBairro").val("...");
                $("#enderecoCidade").val("...");
                $("#enderecoEstado").val("...");

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $("#enderecoRua").val(dados.logradouro);
                        $("#enderecoBairro").val(dados.bairro);
                        $("#enderecoCidade").val(dados.localidade);
                        $("#enderecoEstado").val(dados.uf);
                        $("#enderecoRua").focus();    
                        
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        $("#modalConteudo").html("CEP não encontrado.");
                        $("#myModal").modal()
                        limpa_formulário_cep();
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                $("#modalConteudo").html("Formato de CEP inválido.");
                $("#myModal").modal()
                limpa_formulário_cep();
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    });
});

function limpa_formulário_cep() {
    // Limpa valores do formulário de cep.
    $("#enderecoRua").val("");
    $("#enderecoNumero").val("");
    $("#enderecoComplemento").val("");
    $("#enderecoBairro").val("");
    $("#enderecoCidade").val("");
    $("#enderecoEstado").val("");
    $("#enderecoCep").val("");
    $("$enderecoCep").focus();
}


