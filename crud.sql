-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 21-Jul-2019 às 15:41
-- Versão do servidor: 10.1.38-MariaDB
-- versão do PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crud`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoas`
--

CREATE TABLE `pessoas` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) DEFAULT NULL,
  `sexo` varchar(9) DEFAULT NULL,
  `nascimento` varchar(19) DEFAULT NULL,
  `identidadeNumero` varchar(13) DEFAULT NULL,
  `identidadeOrgaoExpeditor` varchar(7) DEFAULT NULL,
  `identidadeEstadoExpeditor` varchar(2) DEFAULT NULL,
  `cpf` varchar(14) DEFAULT NULL,
  `enderecoRua` varchar(72) DEFAULT NULL,
  `enderecoNumero` int(5) DEFAULT NULL,
  `enderecoComplemento` varchar(18) DEFAULT NULL,
  `enderecoBairro` varchar(25) DEFAULT NULL,
  `enderecoCidade` varchar(24) DEFAULT NULL,
  `enderecoEstado` varchar(2) DEFAULT NULL,
  `enderecoCep` varchar(10) DEFAULT NULL,
  `email_pessoal` text,
  `telefone` varchar(14) DEFAULT NULL,
  `celular` varchar(14) DEFAULT NULL,
  `estadoCivil` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pessoas`
--
ALTER TABLE `pessoas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pessoas`
--
ALTER TABLE `pessoas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
